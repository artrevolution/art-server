#!/bin/bash
#
# Configuration file for publish user on live webservers
#
appversion_file=$HOME/.artrev-appversion
if [ -f "$appversion_file" ]; then
	appversion=`cat $appversion_file`
	export CODEHOME="/publish/app/$appversion"
	
	if [ ! -d "$CODEHOME" ]; then
		echo "CODEHOME is not a directory ($CODEHOME), remove $appversion_file"
		exit 1
	fi
	
	echo "==================================================="
	echo "Currently configured using app version: $appversion"
	echo "==================================================="
	echo "To switch between versions, run one of the following commands:"
	echo 
	echo "	artrev-live artrev-stage artrev-test"
	echo 
	echo "Execution paths will be set up automatically"
fi

art_mode() {
	echo -n $1 > $appversion_file
	dorc
	g
}

alias artrev-live='art_mode live'
alias artrev-stage='art_mode stage'
alias artrev-test='art_mode test'

alias g='cd $CODEHOME'
alias ..='cd ..'
alias ...='cd ../..'

#
# This may be too restrictive of a path, try for now
#
# This MUST be deterministic as this may be run multiple times and it should overwrite previous PATHs
# 
export PATH=/usr/local/bin:/bin:/usr/bin:/usr/local/live-tools/bin:$CODEHOME/bin:$CODEHOME:/usr/local/go/bin

#
# No side-effects until RDS is live
#
pg_rds_master_config() {
	source /etc/artrev.conf

	export PGHOST=$DATABASE_HOST
	export PGPORT=$DATABASE_PORT
	export PGUSER=$DATABASE_MASTER_USER
	export PGPASSWORD=$DATABASE_MASTER_PASSWORD
}



pg_rds_app_config() {
	export PGHOST=$DATABASE_HOST
	export PGPORT=$DATABASE_PORT
	export PGUSER=$DATABASE_APP_USER
	export PGPASSWORD=$DATABASE_APP_PASSWORD
}

source /etc/artrev.conf
source /usr/local/live-tools/bin/bashrc

alias dorc='source ~/.bash_profile'
alias vrc='vi ~/configure/bin/bash_profile-publish.sh && dorc'

#pg_dump_localhost() {
#	env -i pg_dump -F plain --no-privileges -U root $*
#}

#pg_copy_to_rds() {
#	export PGPASSWORD=$DATABASE_APP_PASSWORD
#	env -i pg_dump --clean --no-owner -Fc artrevdb | pg_restore --clean --no-owner --role=app-art -h $DATABASE_HOST -p $DATABASE_PORT -U $DATABASE_APP_USER -d artrevdb
#}

badge() {
        local message
        message="$*"
        printf "\e]1337;SetBadgeFormat=%s\a" $(echo -n "$message" | base64)
}

pg_rds_app_config

alias T-go-live='tail -F $LOG_PATH/go-live/current'
alias T-go-stage='tail -F $LOG_PATH/go-stage/current'
alias T-uwsgi-live='tail -F $LOG_PATH/uwsgi-live/current'
alias T-uwsgi-stage='tail -F $LOG_PATH/uwsgi-stage/current'

alias gits='git status'

g
