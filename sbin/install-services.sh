#!/bin/bash
SVDIR=/etc/service
SOURCEDIR=/publish/configure/host/webserver/services

err() { >&2 printf '%s\n' "$*"; }
fatal() { err "${0##*/}: fatal: $*"; exit 1; }
warn() { err "${0##*/}: warning: $*"; }

install_service() {
	local s
	s=$1
	if [ -d $SVDIR/$s ]; then
		echo "Terminating $SVDIR/$s ..."
		svc -dx $SVDIR/$s || fatal "Unable to terminate service $s"
		rm -rf $SVDIR/$s
		sleep 1
	fi
	if [ ! -f $SOURCEDIR/$s/run ]; then
		warn "$SOURCEDIR/$s/run does not exist, skipping"
	else 
		cp -r $SOURCEDIR/$s $SVDIR
		chown -R root $SVDIR/$s
		find $SVDIR/$s -type f | xargs chmod 600
		find $SVDIR/$s -name run | xargs chmod 700
	fi
}

for c in live stage test; do
	for s in go-$c uwsgi-$c; do 
		install_service $s
	done
done
install_service nginx

echo "Done ... Waiting for daemontools to trigger (5 seconds)"
sleep 5
svstat $SVDIR/* $SVDIR/*/log
